package com.akanksha.demo.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import com.akanksha.demo.entity.User;

@Component
public class UserDao {

	private static List<User> users = new ArrayList<>();
	private int idCounter = 3;
	static {

		users.add(new User(1,"Raman",new Date()));
		users.add(new User(2,"Isha",new Date()));
		users.add(new User(3,"Ruhani",new Date()));
	}
	
	
	
	public List<User> findAllUsers()
	{
		return users;
	}
	
	public User findOneUser(int id)
	{
		for(User u:users)
		{
			if(u.getId()==id)
			{
				return u;
			}
			
				
		}
		return (new User());
		
	}
	
	
	public String addUser(User user)
	{
		if(user.getId()==null)
		{
			user.setId(++idCounter);
		}
		users.add(user);
		return "added successfully";
	}
	
	
	public void deleteuser(int id)
	{
		Iterator<User> it = users.iterator();
		while(it.hasNext())
		{
			User u = it.next();
			if(u.getId()==id)
			{
				it.remove();
			}
		}
	}
}
