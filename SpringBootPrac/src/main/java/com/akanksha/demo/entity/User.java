package com.akanksha.demo.entity;

import java.util.Date;

public class User {
	
	private Integer id;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String name;
	private Date date;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public User(int id, String name, Date date) {
		super();
		this.id = id;
		this.name = name;
		this.date = date;
	}
	

	
}
