package com.akanksha.demo.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mediatype.MessageResolver;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.akanksha.demo.dao.UserDao;
import com.akanksha.demo.entity.User;

@RestController
public class UserController {
	
	@Autowired
	UserDao userdao;
	
	@Autowired
	private MessageSource messageSource;
	
	@RequestMapping(method = RequestMethod.GET,path = "/users")
	public List<User> getAllUsers()
	{
		List<User> users = userdao.findAllUsers();
		return users;
	}
	
	@GetMapping("/user/{id}")
	public EntityModel<User> getUserById(@PathVariable int id)
	{
		
		User user = userdao.findOneUser(id);
		
		EntityModel<User> resource = EntityModel.of(user);
		
		WebMvcLinkBuilder linkTo = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(this.getClass()).getAllUsers());
		
		resource.add(linkTo.withRel("all-users-link"));
		return resource;
	}
	
	@PostMapping("/adduser")
	public String addUser(@RequestBody User u) 
	{
		userdao.addUser(u);
		return null;
	}
	
	@DeleteMapping("/removeuser/{id}")
	public String delete(@PathVariable int id) {
		
		userdao.deleteuser(id);
		return "deleted successfully";
	}
	
	
	@GetMapping("/internationalization")
	public String GoodMorning(@RequestHeader(name="Accept-Language", required = false) Locale locale)
	{
		return messageSource.getMessage("morning.message",null, locale);
	}
	
	

}
